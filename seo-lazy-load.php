<?php
/*
 Plugin Name: SEO Friendly Lazy Loading
 Plugin URI: http://wordpress.org/plugins/seo-lazy-load/
 Description: This plugin adds some simple functions to lazy-load your content while still being SEO friendly.
 Author: Brian Neff
 Version: 1.0.0
 Author URI: http://vius.co/
 */

require( 'SEO_Lazy_Load.class.php' );

//hook the init at 999 so we're one of the last plugins to run on init in case other plugins want to bind actions to this one
add_action( 'init' , array( 'SEO_Lazy_Load' , 'init' ) , 999 );
//hook the ajax requests right before the template loads
add_action( 'template_redirect' , array( 'SEO_Lazy_load' , 'checkRequest' ) , 999 );
//hook the head function to output the meta tag
add_action( 'wp_head' , array( 'SEO_Lazy_Load' , 'wpHead' ) , 0 );
//make sure we localize the script at the footer
add_action( 'wp_footer' , array( 'SEO_Lazy_Load' , 'localizeScript' ) , 0 );

//create the functions

if( !function_exists( 'll_content' ) ) {
	
	/**
	 * @param string $contentId The string ID of the content this block is for.
	 * @param string $classes Any additional classes you want added to the block.
	 * @param string $tag The tag to use for the block.
	 * @param string $mode One of fade, replace, append, prepend, or a custom string of your choosing. This determines how the content is added to the container when loaded via lazy-load or hashbang change. If a custom mode is specified, you must implement that method using the callback system of the JS side of the plugin.
	 * @param string $append Any additional markup to append before the closing > in the opening tag of the block. This is appended in raw form so exactly what you set here will be output in its entirety.
	 */
	function ll_content( $contentId , $classes = '' , $tag = 'div' , $mode = 'fade' , $append = '' ) {
		SEO_Lazy_Load::renderContentBlock( $contentId , $classes , $tag , $mode , $append );
	}
	
}

if( !function_exists( 'll_add_content' ) ) {

	/**
     * @param string $contentId The string ID for this lazy load content. Must be unique or it will overwrite the previous callback.
     * @param string|multitype $callback The callback function to render this lazy load content. Can be passed as a string or an array for object methods: array( $object , 'functionName' )
     * @param number $priority The priority of this content on a page load. Content is loaded from low to high.
     */
	function ll_add_content( $contentId , $callback , $priority = 100 ) {
		SEO_Lazy_Load::addContent($contentId , $callback , $priority );
	}

}

if( !function_exists( 'll_get_fragment' ) ) {

	/**
	 * @return string Returns the fragment portion of the URL.
	 */
	function ll_get_fragment() {
		return SEO_Lazy_Load::getFragment();
	}

}

if( !function_exists( 'll_is_seo_request' ) ) {
	
	function ll_is_seo_request() {
		return SEO_Lazy_Load::_isSEORequest();
	}
	
}