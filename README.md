# SEO Friendly Lazy Loading Plugin #

This plugin easily enables Wordpress developers to lazy load their content while keeping their site SEO friendly. This plugin is designed to work with the "hashbang" style of AJAX url ( foo.html#!some/thing ). Search engines will automatically translate those hashbang urls into "ugly" urls for indexing your content which the plugin is aware of. You should make sure that when using this plugin, you make your content rendering functions aware of the "fragment" of the page/url as that is what should be used to differentiate sections/areas of content on the site.



## Simple Use ##

The usage of the plugin has been simplified to make it easy to get up and running. There are two steps needed for lazy loading content.


### Step 1 - Create the content ###

You must create a function which will render the content you desire. This function will receive a single parameter, the fragment for the current request. A basic setup would be something like this:

**block-mycontent.php**
```
#!php
<h1>Hello World!</h1>
<p>This is some lazy content that will load after the page loads.</p>
```

**inside your theme's functions.php file**
```
#!php
<?php
//this function will include the appropriate block template to render "mycontent"
function mytheme_get_mycontent() {
    get_template_part( 'block' , 'mycontent' );
}

//define the content for lazy loading
ll_add_content( 'mycontent' , 'mytheme_get_mycontent' );

```

You have now created a template file for your content, a function which will include that template file, and defined the content for Lazy Loading.


### Step 2 - Include the content in your main template ###

Now that you've created the content, it's time to include that content in your template. Basically, you want to go to the area where you want that content displayed in your template, and call a lazy load function to tell the plugin where to put that content.

**somewhere in your template files**
```
#!php
<section id="main">
    <?php ll_content( 'mycontent' ); ?>
</section>
```

The only rule to remember here is that the lazy load system uses the content id as the id attribute of the container to load it. This is not namespaced to make it easier for styling with CSS. If, in the example above, you wanted to have the section tag with the ID of mycontent, you would need to use the additional parameters of the ll_content function to achieve this:

```
#!php
<?php ll_content( 'mycontent' , 'add-this-class' , 'section' ); ?>
```

Which would output **<section id="mycontent" class="ll-content add-this-class"></section>** into your template. In many cases you will not need to use this, but it is available if you want to simplify your markup for better semantics.



## Function Overview ##

**ll_content(** $contentId , $classes = '' , $tag = 'div' , $mode = 'replace' , $append = '' **)**

*string* **$contentId** The string ID of the content this block is for.

*string* **$classes** Any additional classes you want added to the block.

*string* **$tag** The tag to use for the block.

*string* **$append** Any additional markup to append before the closing > in the opening tag of the block. This is appended in raw form so exactly what you set here will be output in its entirety.



**ll_add_content(** $contentId , $callback , $priority = 100 **)**

*string* **$contentId** The string ID for this lazy load content. Must be unique or it will overwrite the previous callback.

*string|multitype* **$callback** The callback function to render this lazy load content. Can be passed as a string or an array for object methods: array( $object , 'functionName' )

*number* **$priority** The priority of this content on a page load. Content is loaded from low to high.



**ll_get_fragment()**

Returns the fragment portion of the URL. (Everything after the #! portion of the url)