<?php

if( !class_exists( 'SEO_Lazy_Load' ) ) {

	final class SEO_Lazy_Load {
	
	    const VERSION = '1.0.0';
		
	    private static $_callbacks = array();
	    private static $_priorities = array();
	    private static $_renderedContentBlocks = array();
	    private static $_contentParams = array();
	    
	    public static function init() {
	        //add js side if we're not in the admin
	        if( !is_admin() ) {
	        	wp_enqueue_script('seo-lazy-load',plugins_url('js/seo-lazy-load.js',__FILE__),array('jquery'),'1.0.0',true);
	        }
	        
	    }
		
	    public static function wpHead() {
	    	echo '<meta name="fragment" value="!" />';
	    }
	    
	    /**
	     * This checks the request and executes the content request if needed.
	     */
	    public static function checkRequest() {
	    	//see if this was an ajax request for lazy load content
	    	if( self::_isContentRequest() ) {
	    	
	    		//find out what content we're loading
	    		$contentId = $_REQUEST['ll_content'];
	    	
	    		//run the actions for this content
	    		self::_doContent($contentId);
	    		 
	    		//make sure we stop executing here so wordpress doesn't run
	    		exit;
	    		 
	    	}
	    }
	    
	    /**
	     * @return string Returns the value of the fragment for this request.
	     */
	    public static function getFragment() {
	    	
	    	if( !empty( $_REQUEST['_escaped_fragment_'] ) ) {
	    		//this is for the query var that is being sent by search engines
	    		return urldecode( $_REQUEST['_escaped_fragment_'] );
	    	} else {
	    		//this is for the hashbang being sent with a content request
	    		return urldecode( $_REQUEST['ll_fragment'] );
	    	}
	    	
	    }
	    
	    /**
	 * @param string $contentId The string ID of the content this block is for.
	 * @param string $classes Any additional classes you want added to the block.
	 * @param string $tag The tag to use for the block.
	 * @param string $mode One of fade, replace, append, prepend, or a custom string of your choosing. This determines how the content is added to the container when loaded via lazy-load or hashbang change. If a custom mode is specified, you must implement that method using the callback system of the JS side of the plugin.
	 * @param string $append Any additional markup to append before the closing > in the opening tag of the block. This is appended in raw form so exactly what you set here will be output in its entirety.
	 */
	    public static function renderContentBlock( $contentId , $classes = '' , $tag = 'div' , $mode = 'fade' , $append = '' ) {
	    	
	    	//if the content for this block isn't defined, trigger a warning to the user
	    	if( empty( self::$_callbacks[ $contentId ] ) ) {
	    		trigger_error( 'There is no content defined for "'.$contentId.'". Please remember to call ll_add_content() to set up your content block in your theme\'s functions.php file.' , E_USER_WARNING );
	    	} else {
	    		//content is defined, lets mark it as being used on this page
	    		self::_markContentUsed($contentId);
	    	}
	    	
	    	//opening tag
	    	echo '<'.$tag.' id="'.$contentId.'" class="ll-content';
	    	if( !empty( $classes ) ) echo ' '.$classes;
	    	echo '" data-ll-content-mode="'.$mode.'"';
	    	if( !empty( $append ) ) echo ' '.$append;
	    	echo '>';
	    	
	    	//if this is an SEO request, render the content in place
	    	if( self::_isSEORequest() ) {
	    		self::_doContent($contentId);
	    	}
	    	
	    	//closing tag
	    	echo '</'.$tag.'>';
	    	
	    }
	    
	    /**
	     * @param string $contentId The string ID for this lazy load content. Must be unique or it will overwrite the previous callback.
	     * @param string|multitype $callback The callback function to render this lazy load content. Can be passed as a string or an array for object methods: array( $object , 'functionName' )
	     * @param number $priority The priority of this content on a page load. Content is loaded from low to high.
	     */
	    public static function addContent( $contentId , $callback , $priority = 100 ) {
	    	//add the callback
	    	self::$_callbacks[ $contentId ] = $callback;
	    	//set up the priority data
	    	self::$_priorities[ $contentId ] = $priority;
	    }
	    
	    /**
	     * Localizes the lazy load script to populate it with the lazy load content data.
	     */
	    public static function localizeScript() {
	    	wp_localize_script('seo-lazy-load', '_llParams', self::_buildParams() );
	    }
	    
	    /**
	     * @return multitype:Ambigous Returns an array of the params to send to the client side. 
	     */
	    private static function _buildParams() {
	    	return array(
	    		'onLoad' => !self::_isSEORequest(),
	    		'content' => self::_getLazyLoadContent(),
	    		'fragment' => self::getFragment(),
	    		'fragmentParams' => new stdClass(),
	    		'contentParams' => self::$_contentParams,
	    	);
	    }
	    
	    /**
	     * @return multitype:string Returns an array of the used content blocks on this page in the order they should be loaded.
	     */
	    private static function _getLazyLoadContent() {
	    	//make sure content blocks are sorted by their priority
	    	ksort( self::$_renderedContentBlocks );
	    	//flatten into a single dimensional array
	    	$_return = array();
	    	foreach( self::$_renderedContentBlocks as $blocks ) {
	    		foreach( $blocks as $block ) {
	    			$_return[] = $block;
	    		}
	    	}
	    	return $_return;
	    }
	    
	    /**
	     * @param string $contentId The string ID of the content that is being marked as used.
	     * This function flags a particular piece of content as being used so the lazy load system will load it after the page is rendered.
	     */
	    private static function _markContentUsed( $contentId ) {
	    	//push the content ID into the rendered content blocks information based on priority
	    	self::$_renderedContentBlocks[ self::$_priorities[ $contentId ] ][] = $contentId;
	    }
	    
	    /**
	     * @param string $contentId The strng ID for the content you wish to load.
	     */
	    private static function _doContent( $contentId ) {
	    	//if the content doesn't exist, die with error
	    	if( empty( self::$_callbacks[ $contentId ] ) ) {
	    		trigger_error( 'Content for "'.$contentId.'" is not defined.' , E_USER_WARNING );
	    		die( 'Content for "'.$contentId.'" is not defined.' );
	    	}
	    	//call the function
	    	call_user_func( self::$_callbacks[ $contentId ] , self::getFragment() );
	    }
	    
	    /**
	     * @return boolean Returns true if this is an SEO request with _escaped_fragment_ set. False otherwise.
	     */
	    private static function _isSEORequest() {
	    	return isset( $_REQUEST['_escaped_fragment_'] );
	    }
	    
	    /**
	     * @return boolean True if the current request contained the header for lazy load content. False otherwise.
	     */
	    private static function _isContentRequest() {
	        //look for the header for ajax requests sent through the framework
	        if( !isset( $_SERVER['HTTP_X_REQUESTED_WITH'] ) || $_SERVER['HTTP_X_REQUESTED_WITH'] != 'SEO Lazy Load Content' ) return false;
	        //make sure that we have a content type to get
	        if( empty( $_REQUEST['ll_content'] ) ) return false;
	        return true;
	    }
	
	} //end class
	
} //end if