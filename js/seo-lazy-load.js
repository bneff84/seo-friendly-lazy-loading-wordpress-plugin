( function($) {
	
	var LL = {
		
		_params : _llParams,
		_defaults : {
			fragmentParams : {
				onlyLoadContent : false
			}
		},
		
		init : function() {
			//bind the click listener for all links that have lazyload content params
			$(document).on( 'click' , 'a[data-ll-content]' , function(e) {
				//must use LL here
				LL.onClick( e );
			} );
			
			//set up the onload
			$(document).ready( function(e) {
				//must use LL here
				LL.onLoad( e );
			} );
			
			//setup the hashchange
			$(window).on( 'hashchange' , function(e) {
				//must use LL here
				LL.onHashChange( e );
			} );
		},
		
		onLoad : function( e ) {
			//load the current fragment
			this.updateFragment();
			
			//update any nav items
			this.updateNavItems();
			
			//if we're supposed to load content on page load, do it
			if( this._params.onLoad ) {
				this.loadAllContent();
			}
		},
		
		onClick : function( e ) {
			//load the params for this hash
			var elm = $(e.target);
			var href = elm.attr('href');
			this.setFragmentParams( href.substring( 2 , href.length ) , {
				onlyLoadContent : elm.attr('data-ll-content') ? elm.attr('data-ll-content') : false
			} );
		},
		
		updateNavItems : function( fragment , contentId ) {
			
			//local ref to fragment if we didn't get one with the function
			if( fragment == undefined ) fragment = this._params.fragment;
			
			//set context
			if( contentId ) {
				var context = $('#'+contentId);
			} else {
				var context = $('body');
			}
			
			//clear all active nav items
			context.find('.ll-nav.active').removeClass('active');
			
			//get all the href elements that reference this hash on this page, find their nav parent(s) and then set the active class on them
			context.find('a[href="#!'+fragment+'"] , a[href="'+window.location.href+'"] , a[href="'+window.location.pathname + window.location.search + window.location.hash+'"]').parents('.ll-nav').addClass('active');
		},
		
		setFragmentParams : function( fragment , params ) {
			this._params.fragmentParams[ fragment ] = params;
		},
		
		getFragmentParams : function( fragment ) {
			if( !this._params.fragmentParams || !this._params.fragmentParams[ fragment ] ) return $.extend( {} , this._defaults.fragmentParams );
			return $.extend( {} , this._defaults.fragmentParams , this._params.fragmentParams[ fragment ] );
		},
		
		onHashChange : function( e ) {
			//update the hashbang
			this.updateFragment();
			
			//update the nav items on the page
			this.updateNavItems();
			
			//reload all the content
			this.loadAllContent();
		},
		
		loadAllContent : function() {
			
			//get the content mode we're loading in
			var fragmentParams = this.getFragmentParams( this._params.fragment )
			
			//check to see if we're only loading certain content
			if( fragmentParams.onlyLoadContent ) {
				//process content
				var contents = fragmentParams.onlyLoadContent.split(',');
				for( i=0; i < contents.length; ++i ) {
					this.loadContent( contents[i] , fragmentParams );
				}
			} else {
				for( var i=0; i < this._params.content.length; ++i ) {
					this.loadContent( this._params.content[i] , fragmentParams );
				}
			}
		},
		
		updateFragment : function() {
			this._params.fragment = this.getFragment();
		},
		
		getFragment : function() {
			var hash = window.location.hash;
			if( hash ) {
				if( hash.indexOf('#!') === 0 ) {
					//this is a hashbang fragment
					return hash.substring( 2 , hash.length );
				}
			}
			return '';
		},
		
		loadContent : function( contentId , fragmentParams , error ) {
			
			//set the helper classes
			$('#'+contentId).removeClass('ll-error').addClass('ll-loading');
			
			//closure to maintain scope
			( function( contentId , fragmentParams , error , LL ) {
				
				$.ajax( {
					headers: { 'X-Requested-With':'SEO Lazy Load Content' },
					type: 'POST',
					dataType: 'html',
					data: {
						ll_content: contentId,
						ll_fragment: LL._params.fragment,
					},
					success: function( response ) {
						var target = $('#'+contentId);
						var contentMode = target.attr('data-ll-content-mode');
						//remove the loading class and push the content into the appropriate container
						target.removeClass('ll-loading');
						switch( contentMode ) {
							case 'prepend':
								target.prepend( response );
								//trigger the callback
								LL.doCallback( contentId , contentMode , response );
								break;
							case 'replace':
								target.empty().append( response );
								//trigger the callback
								LL.doCallback( contentId , contentMode , response );
								break;
							case 'append':
								target.append( response );
								//trigger the callback
								LL.doCallback( contentId , contentMode , response );
								break;
							case 'fade':
								LL._fade( target , response , contentId , contentMode );
								//we don't trigger the callback here, because this is a special case
								break;
							//nothing special needed for custom modes
						}
					},
					error: function( jqXHR , textStatus , errorThrown ) {
						$('#'+contentId).removeClass('ll-loading').addClass('ll-error');
						if( error ) error(  jqXHR , textStatus , errorThrown );
					}
				} );
			} )( contentId , fragmentParams , error , this );
			
		},
		
		_fade : function( target , response , contentId , contentMode ) {
			//fade out the container
			target.fadeOut(200,function(){
				//replace the content and fade it back in
				$(this).empty().append( response ).fadeIn(200);
				//now trigger the callback function since we've input the content into the page
				LL.doCallback( contentId , contentMode , response );
			});
		},
		
		doCallback : function( contentId , contentMode , response ) {
			if( this._params.callback ) this._params.callback( contentId , contentMode , response );
		},
		
		setCallback : function( callback ) {
			this._params.callback = callback;
		},
		
	}
	
	window.LL = LL;
	
	//initialize
	LL.init();
	
} )( jQuery );